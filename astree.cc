#include "astree.h"
#include "stringset.h"
#include "lyutils.h"
#include "auxlib.h"

astree::astree (int symbol, size_t filenr, size_t linenr,
                size_t offset, const char* clexinfo):
        symbol (symbol), filenr (filenr), linenr (linenr),
        offset (offset), lexinfo (intern_stringset (clexinfo)) {
   DEBUGF ('f', "astree %p->{%lu:%lu.%lu: %s: \"%s\"}\n",
           (void*) this, filenr, linenr, offset,
           get_yytname (symbol), lexinfo->c_str());
}

astree* adopt1 (astree* root, astree* child) {
   root->children.push_back (child);
   DEBUGF ('a', "%p (%s) adopting %p (%s)\n",
           root, root->lexinfo->c_str(),
           child, child->lexinfo->c_str());
   return root;
}

astree* adopt2 (astree* root, astree* left, astree* right) {
   root = adopt1 (root, left);
   root = adopt1 (root, right);
   return root;
}

astree* adopt3 (astree* root, astree* left, astree* center,
                astree* right) {
   root = adopt1 (root, left);
   root = adopt1 (root, center);
   root = adopt1 (root, right);
   return root;
}

astree* adopt1sym (astree* root, astree* child, int symbol) {
   root = adopt1 (root, child);
   root->symbol = symbol;
   return root;
}

astree* adopt2sym (astree* root, astree* left,
                   astree* right, int symbol) {
   root = adopt1 (root, left);
   root = adopt1 (root, right);
   root->symbol = symbol;
   return root;
}

void sym_swap (astree* node, int symbol) {
   node->symbol = symbol;
}

static void dump_node (FILE* outfile, astree* node) {
   fprintf (outfile, "%p->{%s(%d) %ld:%ld.%03ld \"%s\" [",
            node, get_yytname (node->symbol), node->symbol,
            node->filenr, node->linenr, node->offset,
            node->lexinfo->c_str());
   bool need_space = false;
   for (size_t child = 0; child < node->children.size(); ++child) {
      if (need_space) fprintf (outfile, " ");
      need_space = true;
      fprintf (outfile, "%p", node->children.at(child));
   }
   fprintf (outfile, "]}");
}

static void dump_astree_rec (FILE* outfile, astree* root, int depth) {
   if (root == NULL) return;
   fprintf (outfile, "%*s%s ", depth * 3, "", root->lexinfo->c_str());
   dump_node (outfile, root);
   fprintf (outfile, "\n");
   for (size_t child = 0; child < root->children.size(); ++child) {
      dump_astree_rec (outfile, root->children[child], depth + 1);
   }
}

void dump_astree (FILE* outfile, astree* root) {
   dump_astree_rec (outfile, root, 0);
   fflush (NULL);
}

static void fdump_astree_rec (FILE* outfile, astree* node, int depth) {
   int i;
   if (node == NULL) return;
   
   // Printing out the pipes and spaces to signify the node being a 
   // child of some other node and printing node data.
   for (i = 0; i < depth; ++i) {
      fprintf (outfile, "|   ");
   }
   fprintf (outfile, "%s \"%s\" %lu.%lu.%lu\n",
            get_yytname(node->symbol), (node->lexinfo)->c_str(),
            node->filenr, node->linenr, node->offset);
   
   // Recursively printing the info of the node's children.
   for (size_t child = 0; child < node->children.size(); ++child) {
      fdump_astree_rec (outfile, node->children[child], depth + 1);
   }
}

void fdump_astree (FILE* outfile, astree* root) {
   fdump_astree_rec (outfile, root, 0);
   fflush (NULL);
}

void yyprint (FILE* outfile, unsigned short toknum, astree* yyvaluep) {
   if (is_defined_token (toknum)) {
      dump_node (outfile, yyvaluep);
   }else {
      fprintf (outfile, "%s(%d)\n", get_yytname (toknum), toknum);
   }
   fflush (NULL);
}


void free_ast (astree* root) {
   while (not root->children.empty()) {
      astree* child = root->children.back();
      root->children.pop_back();
      free_ast (child);
   }
   DEBUGF ('f', "free [%p]-> %d:%d.%d: %s: \"%s\")\n",
           root, root->filenr, root->linenr, root->offset,
           get_yytname (root->symbol), root->lexinfo->c_str());
   delete root;
}

void free_ast2 (astree* tree1, astree* tree2) {
   free_ast (tree1);
   free_ast (tree2);
}

