/*
   Name: Benson Liu
   ID: W1236292
   Course: CMPS 104a
   Assignment: 2
*/

// From Standard Library
#include <cstring> // C style string
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <libgen.h>
using namespace std;

// Additional Headers
#include "astree.h"
#include "lyutils.h"
#include "stringset.h"
#include "auxlib.h"

// Variables
string CPP = "/usr/bin/cpp";
string yyin_cpp_command;
FILE* astFile = NULL;
FILE* tokFile = NULL;   // Used as extern variable
FILE* strFile = NULL;

// Open a pipe from the C preprocessor.
// On Failure: Terminates Program.
// On Success: Assigns opened pipe to external variable: FILE* yyin.
void yyin_cpp_popen (const char* filename) {
   yyin_cpp_command = CPP + " " + filename;
   yyin = popen (yyin_cpp_command.c_str(), "r");
   if (yyin == NULL) {
      syserrprintf (yyin_cpp_command.c_str());
      exit (get_exitstatus());
   }
}

void yyin_cpp_pclose (void) {
   int pclose_rc = pclose (yyin);
   eprint_status (yyin_cpp_command.c_str(), pclose_rc);
   if (pclose_rc != 0) set_exitstatus (EXIT_FAILURE);
}

void scan_opts (int argc, char** argv) {
   int opt;
   opterr = 0;
   yy_flex_debug = 0;
   yydebug = 0;
   while ((opt = getopt(argc, argv, "lyD:@:")) != -1) {
      switch (opt) {
         case 'l':
            // yylex() debug flags
            yy_flex_debug = 1;
            break;
         case 'y':
            // yyparse() debug flags
            yydebug = 1;
            break;
         case 'D':
            // Pass this option and its argument to cpp. This is mostly
            //useful as -D__OCLIB_OH__ to suppress inclusion of the
            // code from oclib.oh when testing a program.
            // Pretty much extends the options to pass into the cpp
            //program.
            CPP += " -D " + string (optarg);
            break;
         case '@':
            // Call set_debugflags, and use DEBUGF and DEBUGSTMT for
            // debugging. optarg is an external variable for this
            // getopt function for @:
            set_debugflags (optarg);
            break;
         default:
            errprintf ("%:bad option (%c)\n", optopt);
            break;
      }
   }
   if (optind >= argc) {
      errprintf ("Usage: %s [-ly] [-@ flag . . .] [-D string] "
                 "program.oc\n", get_execname());
      exit (get_exitstatus());
   }
   
   // Filename to be either "-" or the last argv
   // depending on optind and argc size.
   const char* filename = optind == argc ? "-" : argv[optind];
   
   // Pushes the file name into a vector in lyutils.cc
   // and the content is accessed by accessing
   // *scanner_filename(<index>)
   scanner_newfilename (filename);
}

string get_src_name (char* file) {
   // Gets the filename of the source file.
   // It strips the path if there is one.
   // This function should be called right after scan_opts()
   // since scan_opts() pushed the file name onto the
   // included_filenames vector and nothing else is pushed in yet.
   string source_file_name = basename (file);
   size_t extension_pos = source_file_name.find_last_of(".");
   
   if (extension_pos == string::npos) {
      // If there isn't an input file or the file does not have an
      // extension.
      errprintf ("Usage: %s [-ly] [-@ flag . . .] [-D string] "
                 "program.oc\n", get_execname());
      exit (get_exitstatus());
   }
   string file_extension = source_file_name.substr(extension_pos);
   if (file_extension != ".oc") {
      // If the source file does not have an .oc extension.
      errprintf ("Source file does not have the extension .oc\n");
      errprintf ("Usage: %s [-ly] [-@ flag . . .] [-D string] "
                 "program.oc\n", get_execname());
      exit (get_exitstatus());
   }
   
   // Retrieves the file name of the source file.
   return source_file_name.substr(0, extension_pos);
}

int main (int argc, char** argv) {
   string file_string;
   char* file_cstring;
   string src_name;
   
   // Sets the execname to be this program
   set_execname (argv[0]);
   
   // Discovering arguments and opens a pipe to yyin
   scan_opts (argc, argv);
   
   // Converting the filename from const string to char*
   file_string = *scanner_filename(0);
   file_cstring = const_cast<char*>(file_string.c_str());
   
   // Retrieves the source file's name and checks its extension
   src_name = get_src_name(file_cstring);
   
   // Opens two files that have the same name as the source file
   // with extensions .tok and .str
   tokFile = fopen(const_cast<char*>((src_name + ".tok").c_str()), "w");
   strFile = fopen(const_cast<char*>((src_name + ".str").c_str()), "w");
   astFile = fopen(const_cast<char*>((src_name + ".ast").c_str()), "w");
   
   // Opens a pre-processor pipe to external variable: FILE* yyin.
   yyin_cpp_popen (file_cstring);
   DEBUGF ('m', "filename = %s, yyin = %p, fileno (yyin) = %d\n",
           file_cstring, yyin, fileno (yyin));

   // We then call yyparse_astree to retrieves the tokens from the
   // stream and dumps it into the .ast file.
   // fdump_astree() is also partially responsible for generating
   // the .str file when the tree contents are being dumped.
   yyparse();
   DEBUGSTMT ('a', dump_astree (stderr, yyparse_astree); );
   fdump_astree (astFile, yyparse_astree);

   // We then dump the contents of stringset into the .str file.
   DEBUGSTMT ('s', dump_stringset (stderr); );
   dump_stringset (strFile);
   
   // Frees the tree that yyparse_astree is pointing at.
   free_ast(yyparse_astree);
   fclose(tokFile);
   fclose(astFile);
   fclose(strFile);
   
   // Closing the pipe and calling yylex_destroy().
   yyin_cpp_pclose();
   yylex_destroy();
   return get_exitstatus();
}
